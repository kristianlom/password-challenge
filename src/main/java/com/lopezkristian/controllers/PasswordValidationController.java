package com.lopezkristian.controllers;

import com.lopezkristian.Services.IValidatePwds;
import com.lopezkristian.models.ValidatePwds;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/validate")
public class PasswordValidationController {
    private final IValidatePwds typeOneService;
    private final IValidatePwds typeTwoService;

    public PasswordValidationController(IValidatePwds typeOneService, IValidatePwds typeTwoService) {
        this.typeOneService = typeOneService;
        this.typeTwoService = typeTwoService;
    }

    @PostMapping("/typeOne")
    public List<String> validateType1Passwords(@RequestBody ValidatePwds validatePwds) throws IllegalArgumentException {
        if (validatePwds.getType() == 1)
            return typeOneService.validatePwds(validatePwds);
        throw new IllegalArgumentException("Invalid password type.");

    }

    @PostMapping("/typeTwo")
    public List<String> validateType2Passwords(@RequestBody ValidatePwds validatePwds) throws IllegalArgumentException {
        if (validatePwds.getType() == 2)
            return typeTwoService.validatePwds(validatePwds);
        throw new IllegalArgumentException("Invalid password type.");

    }
}
