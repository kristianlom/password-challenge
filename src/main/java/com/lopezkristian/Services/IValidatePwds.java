package com.lopezkristian.Services;

import com.lopezkristian.models.ValidatePwds;

import java.util.List;

public interface IValidatePwds {
	List<String> validatePwds (ValidatePwds password);
}


