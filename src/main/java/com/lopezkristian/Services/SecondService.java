package com.lopezkristian.Services;

import com.lopezkristian.models.ValidatePwds;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class SecondService implements IValidatePwds {
    @Override
    public List<String> validatePwds(ValidatePwds validatePwds) {
        return validatePwds.getPwdLst().stream()
                .filter(password -> password.length() > 8
                        && password.matches(".*[a-z].*")
                        && password.matches(".*[A-Z].*")
                        && !password.matches("^\\d*$")
                        && password.matches(".*[_#@$!{}@~'\"\\[\\]*+;-].*"))
                .collect(Collectors.toList());
    }
}

