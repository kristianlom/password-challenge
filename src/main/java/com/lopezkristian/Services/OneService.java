package com.lopezkristian.Services;

import com.lopezkristian.models.ValidatePwds;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service("OneService")
public class OneService implements IValidatePwds {

    @Override
    public List<String> validatePwds(ValidatePwds validatePwds) {
        return validatePwds.getPwdLst().stream()
                .filter(password -> password.length() > 5
                        && !password.matches("^[a-zA-Z]*$")
                        && !password.matches("^\\d*$")
                        && password.matches(".*[_,!\\[\\]{}@~].*")
                        && !password.matches(".*['\"#$*+;-].*"))
                .collect(Collectors.toList());
    }
}
