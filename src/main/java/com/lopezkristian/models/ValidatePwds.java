package com.lopezkristian.models;

import java.util.List;

public class ValidatePwds {
    private int type;
    private List<String> pwdLst;

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public List<String> getPwdLst() {
        return pwdLst;
    }

    public void setPwdLst(List<String> pwdLst) {
        this.pwdLst = pwdLst;
    }
}

